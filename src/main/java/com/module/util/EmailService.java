package com.module.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;
import java.util.Properties;
import com.module.rest.SmtpDetails;

public class EmailService {
    public static SmtpDetails smtp;

    public static Boolean newEmail(String sub, String body, String to,SmtpDetails smtpDetails) {
        Boolean isProcessed = Boolean.FALSE;
        smtp=smtpDetails;
        // Get system properties
        Properties props = new Properties();

        String MAIL_SMTP_CONNECTIONTIMEOUT ="mail.smtp.connectiontimeout";
        String MAIL_SMTP_TIMEOUT = "mail.smtp.timeout";
        String MAIL_SMTP_WRITETIMEOUT = "mail.smtp.writetimeout";
        String MAIL_SOCKET_TIMEOUT = "60000";
        props.put(MAIL_SMTP_CONNECTIONTIMEOUT, MAIL_SOCKET_TIMEOUT);
        props.put(MAIL_SMTP_TIMEOUT, MAIL_SOCKET_TIMEOUT);
        props.put(MAIL_SMTP_WRITETIMEOUT, MAIL_SOCKET_TIMEOUT);

        props.put("mail.smtp.host", smtpDetails.getHost());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", smtpDetails.getPort());
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

        // Get the default Session object.
        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(smtp.getEmail(), smtp.getPassword());
                    }
                });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(smtpDetails.getFrom() +" <" + smtpDetails.getEmail() + ">"));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(sub);

            message.setText(body);

            // Send message
            Transport.send(message);

        }catch (MessagingException mex) {
            mex.printStackTrace();
        }
        return isProcessed;
    }
}
