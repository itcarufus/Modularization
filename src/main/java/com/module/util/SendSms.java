package com.module.util;

/**
 * Created by System-2 on 1/23/2017.
 */

import com.module.config.ConfigProperties;
import com.module.rest.SmsDetails;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class SendSms {

    private final String module = ConfigProperties.module;
    //define route
    private final String route = "4";

    //Prepare Url
    private URLConnection myURLConnection = null;
    private URL myURL = null;
    private BufferedReader reader = null;

    //Send SMS API
    private String mainUrl = "https://control.msg91.com/api/sendhttp.php?";

    //Prepare parameter string
    private final StringBuilder sbPostData = new StringBuilder(mainUrl);

    public Boolean sendSMS(String mobiles, String message, SmsDetails smsDetails) {
        if (mobiles == null || mobiles.equals("")) {
            return false;
        }
        Boolean isProcessed = Boolean.FALSE;

        //encoding message
        String encoded_message = URLEncoder.encode(message);
        encoded_message = URLEncoder.encode(encoded_message);

        sbPostData.append("authkey=" + smsDetails.getApikey());
        sbPostData.append("&mobiles=" + mobiles);
        sbPostData.append("&message=" + encoded_message);
        sbPostData.append("&route=" + route);
        sbPostData.append("&sender=" + smsDetails.getShortCode());
        sbPostData.append("&module=" + module);
        try {
            //final string
            mainUrl = sbPostData.toString();
            //prepare connection
            myURL = new URL(mainUrl);
            myURLConnection = myURL.openConnection();

            myURLConnection.connect();

            reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));

            String response;
            while ((response = reader.readLine()) != null) {
            }
            reader.close();

            isProcessed = Boolean.TRUE;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isProcessed;
    }

}