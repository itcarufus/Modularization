package com.module.rest.util;

public interface GenericResponse {
    void setMessageType(String message);
    void setMessage(String message);
}
