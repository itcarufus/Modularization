package com.module.requesthandler;

import com.module.bo.request.ChangePasswordBO;
import com.module.bo.request.LoginRequestBO;
import com.module.bo.request.ResetPasswordRequestBO;
import com.module.dao.UsersDAO;
import com.module.dto.response.LoginResponseDTO;
import com.module.bo.response.LoginResponseBO;
import com.module.exception.UserNotFoundException;
import com.module.util.MD5Encode;

import java.sql.SQLException;
import java.util.Date;

public class UserRequestHandler {
    public LoginResponseBO login(LoginRequestBO loginRequestBO)
            throws Exception {
        UsersDAO usersDAO = new UsersDAO();
        String userName = loginRequestBO.getUserName();
        LoginResponseDTO loginResponseDTO = usersDAO
                .getUserDetailsWithName(userName);
        LoginResponseBO loginResponseBO = new LoginResponseBO();
        String encodedPassword = MD5Encode.Encode(loginRequestBO.getPassword());
        String sessionId = usersDAO.getSessionIdForUser(userName, encodedPassword);
        int count = getCount(sessionId);
        Boolean isValidUser = userName.equals(loginResponseDTO.getUserName()) && encodedPassword.equals
                (loginResponseDTO.getPassword());
        if (isValidUser) {
            if (count < 16) {
                Long newSessionId = new Date().getTime();
                if (loginResponseDTO.getSessionId() == null) {
                    usersDAO.updateLogInSessionId(loginResponseDTO.getId(), "|" + String.valueOf(newSessionId) + "|");
                } else {
                    usersDAO.updateLogInSessionId(loginResponseDTO.getId(), loginResponseDTO.getSessionId() + "|" +
                            String.valueOf(newSessionId) + "|");
                }
                loginResponseBO.setSessionId(newSessionId + "@" + loginResponseDTO.getId());
                loginResponseBO.setStatus(loginResponseDTO.getStatus());
                loginResponseBO.setId(loginResponseDTO.getId());
                loginResponseBO.setName(loginResponseDTO.getName());
                loginResponseBO.setEmail(loginResponseDTO.getEmail());
            } else {
                usersDAO.removeSessionId(userName, encodedPassword);
                loginResponseDTO.setSessionId("");
                Long newSessionId = new Date().getTime();
                if (loginResponseDTO.getSessionId() == null) {
                    usersDAO.updateLogInSessionId(loginResponseDTO.getId(), String.valueOf(newSessionId));
                } else {
                    usersDAO.updateLogInSessionId(loginResponseDTO.getId(), loginResponseDTO.getSessionId() + "|" +
                            String.valueOf(newSessionId) + "|");
                }
                loginResponseBO.setSessionId(newSessionId + "@" + loginResponseDTO.getId());
                loginResponseBO.setStatus(loginResponseDTO.getStatus());
                loginResponseBO.setId(loginResponseDTO.getId());
                loginResponseBO.setName(loginResponseDTO.getName());
                loginResponseBO.setEmail(loginResponseDTO.getEmail());
            }
        }
        return loginResponseBO;
    }

    private Integer getCount(String sessionId) {
        int counter = 0;
        if (sessionId != null) {
            for (int i = 0; i < sessionId.length(); i++) {
                if (sessionId.charAt(i) == '|') {
                    counter++;
                }
            }
        }
        return counter;
    }

    public Boolean logout(int userId, String sessionIdOfUser) throws SQLException, UserNotFoundException {

        UsersDAO usersDAO = new UsersDAO();
        String sessionId = usersDAO.getSessionIdForUserId(userId);
        Boolean isLoggedOut = usersDAO.updateSessionId(sessionId, sessionIdOfUser, userId);
        return isLoggedOut;
    }

    public boolean changePassword(ChangePasswordBO changePwdBO) throws Exception {
        UsersDAO appUserDAO = new UsersDAO();

        String encodedOldPassword = MD5Encode.Encode(changePwdBO.getOldPassword());
        String encodedNewPassword = MD5Encode.Encode(changePwdBO.getNewPassword());
        changePwdBO.setOldPassword(encodedOldPassword);
        changePwdBO.setNewPassword(encodedNewPassword);
        Boolean isProcessed = appUserDAO.changePassword(changePwdBO);

        return isProcessed;
    }

    public Boolean resetPassword(ResetPasswordRequestBO resetPasswordRequestBO) throws Exception {
        UsersDAO usersDAO = new UsersDAO();

        String encodedNewPassword = MD5Encode.Encode(resetPasswordRequestBO.getNewPassword());
        resetPasswordRequestBO.setNewPassword(encodedNewPassword);
        Boolean isProcessed = usersDAO.resetPassword(resetPasswordRequestBO);

        return isProcessed;
    }
}
