package com.module.api;

import com.module.bo.request.ChangePasswordBO;
import com.module.bo.request.LoginRequestBO;
import com.module.bo.request.ResetPasswordRequestBO;
import com.module.exception.UserNotFoundException;
import com.module.requesthandler.UserRequestHandler;
import com.module.rest.request.ChangePasswordRequest;
import com.module.rest.request.LoginRequest;
import com.module.rest.request.ResetPasswordRequest;
import com.module.rest.response.LoginResponse;
import com.module.bo.response.LoginResponseBO;
import com.module.rest.util.MessageResponse;
import com.module.rest.util.ResponseGenerator;
import com.module.util.UserRequestValidation;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

@Path("/user")
public class UserService {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login")
    public Response login(LoginRequest loginRequest) {
        LoginRequestBO loginRequestBO = new LoginRequestBO();
        loginRequestBO.setUserName(loginRequest.getUserName());
        loginRequestBO.setPassword(loginRequest.getPassword());
        UserRequestHandler userRequestHandler = new UserRequestHandler();
        LoginResponse loginResponse = new LoginResponse();
        MessageResponse messageResponse = new MessageResponse();
        try {
            LoginResponseBO loginResponseBO = userRequestHandler.login(loginRequestBO);
            loginResponse.setName(loginResponseBO.getName());
            loginResponse.setEmail(loginResponseBO.getEmail());
            loginResponse.setStatus(loginResponseBO.getStatus());
            loginResponse.setSessionId(loginResponseBO.getSessionId());
            loginResponse.setId(loginResponseBO.getId());
            if (loginResponseBO.getSessionId() != null && loginResponseBO.getStatus().equals("A")) {
                return ResponseGenerator.generateSuccessResponse(loginResponse, String.valueOf(loginResponseBO.getSessionId()));
            } else {
                return ResponseGenerator.generateFailureResponse(messageResponse, "Invalid password or inactive user.");
            }
        }catch (Exception e) {
            e.printStackTrace();
            return ResponseGenerator.generateFailureResponse(messageResponse, "Invalid Log in");
        }

    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(@HeaderParam("sessionId") String sessionId) {
        LoginResponse loginResponse = new LoginResponse();
        MessageResponse messageResponse = new MessageResponse();
        try {
            UserRequestHandler userRequestHandler = new UserRequestHandler();
            String[] sessionIdParts = sessionId.split("@");
            Boolean isLoggedOut = userRequestHandler.logout(Integer.parseInt(sessionIdParts[1]), sessionIdParts
                    [0]);
            if (isLoggedOut) {
                return ResponseGenerator.generateSuccessResponse(loginResponse, "Log out successfully.");
            } else {
                return ResponseGenerator.generateFailureResponse(messageResponse, "Unable to log out the current user.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseGenerator.generateFailureResponse(messageResponse, "Unable to log out the current user.");
        } catch (UserNotFoundException e) {
            return ResponseGenerator.generateFailureResponse(messageResponse, "Invalid user id");
        }
    }

    @POST
    @Path("/changePassword")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePassword(ChangePasswordRequest changePwdReq) {
        ChangePasswordBO changePwdBO = new ChangePasswordBO(
                changePwdReq.getUserId(), changePwdReq.getOldPassword(),
                changePwdReq.getNewPassword());

        UserRequestHandler appUserRequestHandler = new UserRequestHandler();
        MessageResponse loginResponse = new MessageResponse();
        try {
            if (appUserRequestHandler.changePassword(changePwdBO)) {
                return ResponseGenerator.generateSuccessResponse(loginResponse, "Password updated.");
            } else {
                return ResponseGenerator.generateFailureResponse(loginResponse, "Password update failed.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseGenerator.generateFailureResponse(loginResponse, "Password update failed.");
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/resetPassword")
    public Response changePassword(ResetPasswordRequest resetPasswordRequest, @HeaderParam("Auth") String auth) throws Exception {
        if (auth != null && UserRequestValidation.isRequestValid(auth)) {
            ResetPasswordRequestBO resetPasswordRequestBO = new ResetPasswordRequestBO(
                    resetPasswordRequest.getId(),
                    resetPasswordRequest.getNewPassword());
            UserRequestHandler userRequestHandler = new UserRequestHandler();
            MessageResponse messageResponse = new MessageResponse();
            if (userRequestHandler.resetPassword(resetPasswordRequestBO)) {
                return ResponseGenerator.generateSuccessResponse(messageResponse, "Password has been reset successfully.");
            } else {
                return ResponseGenerator.generateFailureResponse(messageResponse, "Reset password failed.");
            }
        } else {
            return ResponseGenerator.generateResponse(UserRequestValidation.getUnautheticatedResponse());
        }
    }
}
